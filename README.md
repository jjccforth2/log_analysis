# Log Analysis



## Description
This script is for InfoHort project. 
Due to some unknown reason, the production server has a lot of discruption after Spring/Hibernate upgrade without code change. It's difficult to 
duplicate in local/other environment. Checking the logs is an approach to investigate the problem. But it's difficult and tedious to review large
amount of records.

This is a utility to analyze the WebLogic logs to generate needed report. It's supposed to extendable according to the investigation requirement


## Usage
Copy the unziped logs under the data foler and run the app,


## Roadmap
Add more functions to the utility 




