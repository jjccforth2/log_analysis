import sys
import unittest
from infohortAnalysis import process_accesslog , process_access_in_folder#, process_infohortlog

#folder = "data/logsMay09-May17/extracted"
folder = "data"
class TestProcess(unittest.TestCase):

    def test_isupper(self):
        print("test2a")
        fn = "tmp.log"
        filename = f"{folder}/{fn}"
        with open(filename, "w") as f:
            f.write("test")
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_parse_assess(self):
        fn = "access.log-20220510"
        filename = f"{folder}/{fn}"
        p,np = process_accesslog(filename)
        print(f"p:{p} np:{np}")

    def test_parse_assess_folder(self):
        folder = "data/logsMay09-May17/extracted"
        folerout = "data/logsMay09-May17/processed"
        #folder = "data/logsJun14-Jul16/extracted"
        #folerout = "data/logsJun14-Jul16/processed"
        process_access_in_folder(folder, folerout)
        print("End of test_parse_assess_filder")

    def test_parse_assess_folder_uat(self):
        folder = "data/uatlogs/extracted"
        folerout = "data/uatlogs/processed"
        process_access_in_folder(folder, folerout, True)
        print("End of test_parse_assess_filder")

if __name__ == '__main__':
    unittest.main()

