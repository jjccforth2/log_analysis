
import re
import json



def proccess_silverlog(file):
    lines = []
    statsAll = []
    fseg = file.split("/")
    fnopath = fseg[-1]

    with open(file, "r") as f:
        statsOn = False
        stats = {}
        for line in f:
            matchStart = re.search(r"####<([^>]+)>", line, re.M)
            if matchStart:
                dt = matchStart.group(1)
                if statsOn:
                    statsOn = False
                    collectStats = ''.join(stats['data'])
                    stats['data'] = collectStats
                    statsAll.append(stats)
                    stats = {}

                print(dt)
                matchStats = re.search(r"Session Metrics", line, re.M)
                if matchStats:
                    statsOn = True
                    stats['dt'] = dt
                    stats['data'] = []

                continue
            stats['data'].append(line)


        #matchObj = re.search(r'%(\w\w)', line, re.M)
        #if not matchObj:
        #    continue
        #hexv = matchObj.group(1)
        #print(f">>>hex:{hexv}")

        #print(line)
    return statsAll


def proccess_silverlog_th(file):
    statsAll = []

    f = open(file, "r")

    fh = {}

    fseg = file.split("/")
    fnopath = fseg[-1]
    targetThread = 18
    with open(file, "r") as f:
        for line in f:
            matchStart = re.search(r"####<([^>]+)>", line, re.M)
            if not matchStart:
                continue
            matchThread = re.search(r"Thread:\s+'(\d+)", line, re.M)
            if not matchThread:
                continue
            thread = matchThread.group(1)
            if fh.get(thread) is None:
                fh[thread] = open(f"data/threads/{fnopath}-{thread}.log", "w")
            
            seg = line.split("> <")
            shortline = seg[1] + "  " + seg[5][1:7] + ">" + seg[11]
            if not seg[1].startswith("Noti"):
                shortline = seg[0] + " " + shortline
            fh[thread].write(shortline)
            #if thread != f"{targetThread}":
            #    continue
            #print(line)

    for k in fh:
        fh[k].close()



    return


def process_accesslog(file, fileout=None, report_only=False):
    '''
    In access log there are many ping requests. Filter out the ping requests
    Get: unique IP and their activitis and generated clean access log
    '''
    fseg = file.split("/")
    fnopath = fseg[-1]
    ping_ip = {}
    none_ping_ip = {}
    code_stats = {}

    line_to_write = ""
    with open(file, "r") as f:
        for line in f:
            matchIp = re.search(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", line, re.M)
            if " \"GET /Infohort5 HTTP/1.1\"" in line:
                if not matchIp:
                    print("### ERROR, no IP but GET /Infohort5")
                    continue
                ip = matchIp.group(0)
                if ping_ip.get(ip) is None:
                    ping_ip[ip] = 0
                ping_ip[ip] += 1
            else:
                # now we have a none ping request
                if not matchIp:
                    print("### ERROR, no IP but GET not a ping")
                    line_to_write += line 
                    continue
                ip = matchIp.group(0)
                if none_ping_ip.get(ip) is None:
                    none_ping_ip[ip] = 0
                none_ping_ip[ip] += 1
                line_to_write += line 
                matchCode = re.search(r"HTTP/1.1\" (\d+)", line, re.M)
                if matchCode:
                    code = matchCode.group(1)
                    if code_stats.get(code) is None:
                        code_stats[code] = 0
                    code_stats[code] += 1

    if not report_only:
        with open(fileout, "w") as f:
            f.write(line_to_write)
    return ping_ip, none_ping_ip, code_stats   

def process_access_in_folder(folder, outfolder, report_only=False):
    import os
    for file in os.listdir(folder):
        if file.startswith("access"):
            filepath = os.path.join(folder, file)
            ofilepath = os.path.join(outfolder, file)
            ping_ip, none_ping_ip,code_data = process_accesslog(filepath, ofilepath, report_only)
            print(f"file: {file},ping: {ping_ip}, none_ping: {none_ping_ip}, code: {code_data}")



def main():
    file = "data/silver.log-20220715"
    statsAll = proccess_silverlog(file)
    fseg = file.split("/")
    fnopath = fseg[-1]
    with open(f"data/{fnopath}.json", "w+") as f:
        json.dump(statsAll, f, indent=4)

def test1():
    with open(f"data/sample.json", "r") as f:
        d = json.load(f)
    data = d['data']
    lines = data.split("\n")
    for line in lines:
        if line.startswith("}>"):
            continue
        if len(line) == 0:
            continue
        line = line.lstrip()
        #guarenteed to have a match 
        match = re.match(r"^(\d+)\s", line)
        tm = match.group(1)

        print (match.group(1))

if __name__ == "__main__":
    #main()
    #test1()
    files = ["data/silver/silver.log-20220713","data/silver/silver.log-20220714", "data/silver/silver.log-20220715", "data/silver/silver.log-20220716"]
    #file = "data/silver/silver.log-20220714"
    for file in files:
        proccess_silverlog_th(file)
    #data = "####<Jul 12, 2022 9:35:27 AM EDT> <Notice> <Stdout> <spnhlapp003.agr.gc.ca> <silver> <[ACTIVE] ExecuteThread: '5' for queue: 'weblogic.kernel.Default (self-tuning)'> <<WLS Kernel>> <> <> <1657632927853> <BEA-000000> <[2022-07-12 09:35:27,853] INFO   engine.internal.StatisticalLoggingSessionEventListener:258  -> Session Metrics {"

    #seg = data.split("> <")
    #for i, s in enumerate(seg):
    #    print(f"{i}:{s}")
    #print ('\n'.join(seg))